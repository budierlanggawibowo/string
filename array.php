<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Array</title>
</head>
<body>
  <h1>Berlatih Array</h1>
  <?php
  echo "<h3> Soal 1 </h3>";

  // Define the arrays of kids and adults
  $kids = array("Mike", "Dustin", "Will", "Lucas", "Max", "Eleven");
  $adults = array("Hopper", "Nancy", "Joyce", "Jonathan", "Murray");

  echo "<h3> Soal 2</h3>";

  echo "Cast Stranger Things: ";
  echo "<br>";

  echo "Total Kids: " . count($kids) . "<br>";
  echo "<ol>";
  foreach ($kids as $kid) {
    echo "<li>$kid</li>";
  }
  echo "</ol>";

  echo "Total Adults: " . count($adults) . "<br>";
  echo "<ol>";
  foreach ($adults as $adult) {
    echo "<li>$adult</li>";
  }
  echo "</ol>";

  echo "<h3> Soal 3</h3>";

  // Define the multidimensional associative array of characters
  $characters = array(
    array(
      "Name" => "Will Byers",
      "Age" => 12,
      "Aliases" => "Will the Wise",
      "Status" => "Alive"
    ),
    array(
      "Name" => "Mike Wheeler",
      "Age" => 12,
      "Aliases" => "Dungeon Master",
      "Status" => "Alive"
    ),
    array(
      "Name" => "Jim Hopper",
      "Age" => 43,
      "Aliases" => "Chief Hopper",
      "Status" => "Deceased"
    ),
    array(
      "Name" => "Eleven",
      "Age" => 12,
      "Aliases" => "El",
      "Status" => "Alive"
    )
  );

  echo "<table>";
  echo "<tr><th>Name</th><th>Age</th><th>Aliases</th><th>Status</th></tr>";
  foreach ($characters as $character) {
    echo "<tr>";
    echo "<td>{$character['Name']}</td>";
    echo "<td>{$character['Age']}</td>";
    echo "<td>{$character['Aliases']}</td>";
    echo "<td>{$character['Status']}</td>";
    echo "</tr>";
  }
  echo "</table>";
  ?>
</body>
</html>